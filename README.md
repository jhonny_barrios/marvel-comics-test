# Marvel Comic Test #

Este app fue creado como prueba.


### Características ###

* Consumo de api de marvel.
* Listado de 30 comics random.
* Cambio de grid a list y viceversa.
* Posibilidad de añadir cualquier comic a tus favoritos.
* Vista en detalle de comic estilo material design.
* Búsqueda de comic especifico desde el toolbar.
* Pantalla de perfil.
* Pantalla de favoritos.
* Login con facebook.

### Screenshots:###

![WhatsApp Image 2016-12-18 at 4.39.42 PM.jpeg](https://bitbucket.org/repo/E5xL8x/images/1880993973-WhatsApp%20Image%202016-12-18%20at%204.39.42%20PM.jpeg)

![WhatsApp Image 2016-12-18 at 4.39.42 j PM.jpeg](https://bitbucket.org/repo/E5xL8x/images/1650585857-WhatsApp%20Image%202016-12-18%20at%204.39.42%20j%20PM.jpeg)

![WhatsApp Image 2016-12-18 at 4.39.42 jkPM.jpeg](https://bitbucket.org/repo/E5xL8x/images/194710843-WhatsApp%20Image%202016-12-18%20at%204.39.42%20jkPM.jpeg)